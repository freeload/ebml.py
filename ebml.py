from subreader import SubReader

# Index of most significant bit (from the right 0..n)
def msb(n):
    i = 0
    while n != 0:
        n >>= 1
        i += 1
    return i - 1


def readvint(h):
    size = 0
    while True:
        b0 = h.read(1)[0]
        if b0 != 0:
            break
        size += 8
    
    msbidx = msb(b0)
    size += 7 - msbidx

    n = b0 ^ (1 << msbidx)
    for b in h.read(size):
        n = n << 8 | b
    
    return n


def encodeint(n):
    b = []
    while True:
        b.insert(0, n & 0xff)
        n = n >> 8
        if n == 0:
            return b


def encodevint(n):
    b = encodeint(n)
    while True:
        zeroes = (len(b) * 9 - 8) // 8
        mask = 2 ** (7 - zeroes % 8)
        if b[0] < mask:
            b = [(b[0] | mask)] + b[1:]
            prefix = [0] * (zeroes // 8)
            return bytes(prefix + b)
        b = [0] + b


def iterelements(h):
    while not h.eof():
        id_ = readvint(h)
        size = readvint(h)
        offset = h.tell()
        yield id_, SubReader(h, offset, size)
        h.seek(offset+size)


def itertree(h, types):
    for id_, subh in iterelements(h):
        element = types[id_]
        id_, eltype = element
        if eltype == "master":
            yield id_, eltype, itertree(subh, types)
        else:
            yield id_, eltype, subh


#def writetree(iter_, h):
#    totsize = 0
#    for id_, eltype, subh:
#        totsize += size
