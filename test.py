from pprint import pprint

def testvint(n):
    import ebml
    from io import BytesIO
    encoded = ebml.encodevint(n)
    decoded = ebml.readvint(BytesIO(encoded))
    print(repr((n, encoded, hex(n))))
    if n != decoded:
        print(repr((x, decoded, encoded, hex(n))))
        raw_input("ERROR: PRESS ENTER TO CONTINUE")


def testvints():
    for i in range(0,100):
        testvint(2**i - 1)
        testvint(2**i)
        testvint(2**i + 1)


def testmatroska(path):
    from matroska import itermusic
    from collections import defaultdict
    
    tags = defaultdict(set)

    for path, t in itermusic(path):
        for name,value in t.items():
            
            tags[name].add(value)
    
    pprint(tags)


def testeditor():
    import editor
    editor.main()

if __name__ == "__main__":
    testeditor()
#    import sys
#    testmatroska(sys.argv[1])
