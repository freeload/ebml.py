class OverrunError(EOFError): pass

class SubReader(object):
    def __init__(self, parent, offset, limit):
        self.limit = limit
        self.offset = offset
        self.parent = parent
        self.pos = 0
    
    def eof(self):
        return self.pos >= self.limit

    def read(self, size=None):
        if size is None:
            size = self.limit - self.pos

        if self.pos + size > self.limit:
            raise OverrunError("Self overrun")
        
        self.parent.seek(self.offset + self.pos)
        c = self.parent.read(size)
        self.pos += size

        if len(c) != size:
            raise OverrunError("Parent overrun")
        
        return c

    def seek(self, pos):
        if pos == self.pos:
            return
        if pos < 0 or pos > self.limit:
            raise OverrunError("Self overrun")
        
        self.pos = pos

    def tell(self):
        return self.pos


class FakeReader(object):
    def __init__(self, handler):
        self.handler = handler
        self.pos = handler.tell()
        self.first = self.pos
        self.name = handler.name

    def seek(self, pos, *args, **kwargs):
        self.handler.seek(pos, *args, **kwargs)
        pos = self.tell()
        if self.pos != pos:
            self.status()
            self.first = pos
        self.pos = pos

    def tell(self, *args, **kwargs):
        return self.handler.tell(*args, **kwargs)

    def read(self, size, *args, **kwargs):
        c = self.handler.read(size, *args, **kwargs)
        self.pos = self.tell()
        return c

    def status(self):
        print("Read %d-%d (%d)" % (self.first, self.pos, self.pos-self.first))
