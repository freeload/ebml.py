import os

from ebml import itertree
from itertools import chain
from matroskatypes import typedict
from subreader import SubReader


flatten = chain.from_iterable
def itermatroska(fileobj):
    size = os.path.getsize(fileobj.name)
    f = SubReader(fileobj, 0, size)
    for x in itertree(f, typedict):
        yield x


def gettags(iter_):
    tags = {}
    
    iter_ = flatten(data for id_,_,data in iter_ if id_ == "Segment")
    
    foundinfo = False
    foundtags = False
    for id_,_,segdata in iter_:
        if id_ == "Info":
            foundinfo = True
            infodata = (data for id_,_,data in segdata if id_ == "Title")
            for titledata in infodata:
                tags["TITLE"] = titledata.read().decode("utf-8")
            
            if foundtags:
                break
            foundinfo = True

        if id_ == "Tags":
            tagdata = flatten(data for id_,_,data in segdata if id_ == "Tag")
            simpletagdata = (data for id_,_,data in tagdata if id_ == "SimpleTag")

            for tag in simpletagdata:
                tagname = None
                tagstring = None
                for id_,_, c in tag:
                    if id_ == "TagName":
                        tagname = c.read().decode("utf-8")
                    elif id_ == "TagString":
                        tagstring = c.read().decode("utf-8")
                if tagname is not None and tagstring is not None:
                    tags[tagname.upper()] = tagstring
            
            if foundinfo:
                break
            foundtags = True
        
    return tags


def itermusic(path):
    for basepath,_,files in os.walk(path):
        for fname in files:
            if fname[-3:] != "mkv":
                continue
            full = os.path.join(basepath, fname)
            with open(full, "rb", buffering=0) as f:
                yield full, gettags(itermatroska(f))

